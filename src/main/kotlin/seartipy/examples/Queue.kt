package seartipy.examples

class Queue {
    private var head: Node? = null
    private var tail: Node? = null
    private var length: Int = 0

    fun addLast(element: Int) {
        var node = Node(element, null)
        var t = tail
        if (t != null) {
            t.next = node
            tail = null
        } else {
            head = node
            tail = node
        }
        length++

    }

    fun isEmpty() = length == 0

    fun size() = length

    fun removeFirst(): Int? {
        val first = head
        if (first != null) {
            head = first.next
            if (head == null)
                tail = null
            length--
            return first.data
        } else {
            return null
        }
    }

    fun print() {
        var node = head
        while (node != null) {
            var result = node.data
            print(result)
            node = node.next
        }
    }

    fun max(): Int? {
        var node = head
        while (node != null) {
            var result = node.data
            if (result < node.data) {
                result = node.data
                node = node.next
            }
            return result
        }
        return null
    }

    fun peek(): Int? {
        val node = head
        if (node == null) {
            return null
        }
        return node.data
    }
}
