package seartipy.examples

class Stack {
    private var top: Node? = null
    private var length: Int = 0

    fun push(value: Int) {
        top = Node(value, top)
        ++length
        }

    fun isEmpty() = length == 0

    fun size() = length

    fun peek(): Int? {
        val node = top ?: return null
        return node.data
     }

    fun pop(): Int? {
        val node = top
        if(node != null) {
            top = node.next
            --length
            return node.data
        }
        return null
    }

    fun print() {
        var node = top
        while(node != null) {
            val result = node.data
            print(result)
            node = node.next
        }
    }

    fun max():Int? {
        var node = top
        while(node != null) {
            var result = node.data
            if(result < node.data) {
                result = node.data
                node.next
            }
            return result
        }
        return null
    }
}

