package seartipy.examples

class DoubleLinkedList {
    private var head: DoubleNode? = null
    private var tail: DoubleNode? = null
    private var length: Int = 0

    val first: Int?
        get() = head?.data

    val last: Int?
        get() = tail?.data

    val firstNode: DoubleNode?
        get() = head

    val lastNode: DoubleNode?
        get() = tail

    fun isEmpty() = head == null

    fun size() = length


    fun addFirst(value: Int) {
        val node = DoubleNode(data = value, next = head, prev = null)
        val first = head
        if (first != null) {
            first.prev = node
            head = node
        } else {
            head = node
            tail = node
        }
        ++length
    }

    fun addLast(value: Int) {
        val node = DoubleNode(data = value, next = null, prev = tail)
        val last = tail
        if (last != null) {
            last.next = node
            tail = node
        } else {
            head = node
            tail = node
        }
        ++length
    }

    fun toList(): MutableList<Int> {
        val list = mutableListOf<Int>()
        var temp = head
        while (temp != null) {
            list.add(temp.data)
            temp = temp.next
        }
        return list
    }

    fun removeFirst(): Int? {
        val first = head
        if (first != null) {
            first.next?.prev = null
            val result = first.data
            head = first.next
            tail = null
            length--
            return result
        }
        return null
    }

    fun removeLast(): Int? {
        val last = tail
        if (last != null) {
            last.prev?.next = null
            val result = last.data
            tail = last.prev
            length--
            return result
        }
        return null
    }

    fun findAt(value: Int): DoubleNode? {
        var temp = head
        while (temp != null) {
            if (temp.data == value) {
                return temp
            }
            temp = temp.next
        }
        return null
    }

    fun remove(node: DoubleNode): Int {
        node.next?.prev = node.prev
        node.prev?.next = node.next
        if (head == node)
            head = node.next
        if (tail == node)
            tail = node.prev
        length--
        return node.data
    }

    fun nodeAt(index: Int): DoubleNode? {
        var temp = head
        var i = 0
        while (temp != null) {
            if (i == index)
                return temp
            i++
            temp = temp.next
        }
        return null
    }

    fun insert(value: Int, at: DoubleNode) {
        val newnode = DoubleNode(value, at, at.prev)
        at.prev?.next = newnode
        at.prev = newnode
        if (at == head)
            head = newnode
        length++
    }

    fun insertAfter(value: Int, at: DoubleNode) {
        val node = DoubleNode(value, at.next, at)
        at.next?.prev = node
        at.next = node
        if (at == tail)
            tail = node
        length++
    }

}

