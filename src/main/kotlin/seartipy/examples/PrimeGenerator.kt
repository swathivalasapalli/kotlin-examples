package seartipy.examples

fun isPrime(n: Int): Boolean {
    for(i in 2 until n)
        if(n % i == 0)
            return false
        return true
}

class PrimeGenerator {
    private var n: Int
    private var i: Int = 1
    private var nextPrime = 2

    constructor(n: Int) {
        this.n = n
    }

    fun hasNext() = i < n

    fun next(): Int {
        while (true) {
            if (isPrime(nextPrime)) {
                ++i
                ++nextPrime
                return nextPrime - 1
            }
            ++nextPrime
        }
    }
}
    fun main(args: Array<String>) {
        val pg = PrimeGenerator(10)
        while (pg.hasNext())
            println(pg.next())
        }
