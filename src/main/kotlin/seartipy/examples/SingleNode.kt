package seartipy.examples

class SingleNode(var data: Int, var next: SingleNode?)

fun first(node: SingleNode): Int {
    return node.data
}
fun insertAfter(value: Int, node: SingleNode): SingleNode {
    val result = SingleNode(value, node.next)
    node.next = result
    return result
}

fun removeAfter(node: SingleNode) {
    node.next = node.next?.next
}

fun createList(arr: Array<Int>): SingleNode? {
    if(arr.isEmpty())
        return null
    val list = SingleNode(arr.first(), null)
    var t = list
    for(i in 1 until arr.size)
        t = insertAfter(arr[i], t)
    return t
}

fun length(list: SingleNode): Int? {
    var temp: SingleNode? = list
    var count = 0
    while (temp != null) {
        count++
        temp = temp.next
    }
    return count
}


fun printList(list: SingleNode) {
    var temp: SingleNode? = list
    while (temp != null) {
        print(temp.data)
        temp = temp.next
    }
}

fun sum(list: SingleNode): Int {
        var t: SingleNode = list
        var s = 0
        while (t != null) {
            s += t.data
            t = t.next!!
        }
        return s
}

fun reverse(list: SingleNode): SingleNode {
    var prev: SingleNode = list
    var current = list.next
    while (current != null) {
        var temp = current.next
        prev = current
        current = temp
    }
    return prev
}
