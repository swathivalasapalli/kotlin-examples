package seartipy.examples

class FibonacciGenerator {
    private var n: Int
    private var i: Int = 0
    private var lo: Int = 0
    private var hi: Int = 1

    constructor(n: Int) {
        this.n = n
    }

    fun hasNext() = i < n

    fun next(): Int {
        while (true) {
            val temp = lo + hi
            i++
            lo = hi
            hi = temp
            return lo
        }
    }
}

    fun main(args:Array<String>) {
        val fg = FibonacciGenerator(10)
        while(fg.hasNext())
            println(fg.next())
    }

