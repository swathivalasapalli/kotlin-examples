package seartipy.examples

class ArrayList {
    private var arr: IntArray = IntArray(20)
    private var length: Int = 0

    val first: Int?
        get()= arr[0]

    val last: Int?
        get()= arr[length - 1]

    fun add(value: Int) {
        ensureCapacity(arr.size)
        arr[length] = value
        ++length
    }

    fun isEmpty() = length == 0

    val size: Int
        get() = length

    fun pop(): Int? {
        if (length == 0)
            return null
        --length
        return arr[length]
    }

    private fun copy(src: IntArray, dest: IntArray) {
        for(i in src.indices){
            dest[i]= src[i]
        }
    }

    operator fun get(index: Int): Int? {
        require(index in 0 until length)
        return arr[index]
    }

    operator fun set(index: Int, value: Int) {
        require(index in 0 until length)
        arr[index] = value
    }

   private fun ensureCapacity(newCapacity: Int) {
        if(arr.size > newCapacity){
            return
        }
        else {
            val capacity = arr.size
            val temp = IntArray(Math.max(newCapacity, capacity * 2 + 1))
            copy(arr, temp)
            arr = temp
        }
    }

    fun insertAt(value: Int, at: Int) {
        ensureCapacity(length + 1)
        for(i in length downTo at + 1) {
            arr[i] = arr[i - 1]
        }
        arr[at] = value
        ++length
    }

    fun removeAt(at: Int): Int? {
        require(at in 0..arr.size)
        val temp = arr[at]
        length--
        var i = at
        while(i <= length-1){
            arr[i] = arr[i + 1]
            i++
        }
        return temp
    }

    fun toList(): List<Int> {
        val temp = mutableListOf<Int>()
        for(i in 0 until length){
            temp.add(arr[i])
        }
        return temp
    }

}













