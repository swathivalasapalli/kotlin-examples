package seartipy.examples
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import org.junit.Test as test

class StackTest {
    @test
    fun isEmptyTest() {
        val stack = Stack()
        assertEquals(1, 1)
        assertTrue { stack.isEmpty() }
        assertEquals(0,stack.size())
        stack.push(100)
        stack.push(200)
        assertEquals(2, stack.size())
        assertEquals(2, stack.size())
        assertFalse { stack.isEmpty() }
    }

    @test
    fun pushTest() {
        val stack = Stack()
        stack.push(100)
        stack.push(200)
        stack.push(300)
        stack.push(400)
        assertEquals(4,stack.size())
        assertFalse { stack.isEmpty() }
    }

    @test
    fun peekTest() {
        val stack = Stack()
        assertEquals(null, stack.peek())
        stack.push(400)
        stack.push(500)
        assertEquals(500, stack.peek())
    }

    @test
    fun popTest() {
        val stack = Stack()
        stack.push(100)
        stack.push(200)
        stack.push(300)
        assertEquals(300,stack.pop())
        stack.push(400)
        assertEquals(400,stack.pop())
        assertEquals(200, stack.pop())
        assertEquals(100, stack.pop())
        assertEquals(null, stack.pop())
    }

    @test
    fun maxTest() {
        val stack = Stack()
        stack.push(100)
        stack.push(200)
        stack.push(300)
        assertEquals(300, stack.max())
    }

}
