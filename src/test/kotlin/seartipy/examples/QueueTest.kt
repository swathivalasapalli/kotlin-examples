package seartipy.examples

import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import org.junit.Test as test

class QueueTest {
    @test
    fun isEmpty() {
        val queue = Queue()
        assertTrue { queue.isEmpty() }
        queue.addLast(100)
        assertEquals(1, queue.size())
   }

    @test
    fun addLastTest() {
        val queue = Queue()
        assertTrue{queue.isEmpty()}
        queue.addLast(100)
        queue.addLast(200)
        queue.addLast(300)
        queue.addLast(400)
        assertEquals(4, queue.size())
        assertFalse { queue.isEmpty() }
    }

    @test
    fun removeFirstTest() {
        val queue = Queue()
        assertTrue { queue.isEmpty() }
        queue.addLast(100)
        queue.addLast(200)
        queue.addLast(300)
        assertEquals(100,queue.removeFirst())
        assertEquals(200,queue.removeFirst())
        assertEquals(300,queue.removeFirst())
        assertEquals(null,queue.removeFirst())
    }

    @test
    fun maxTest() {
        val queue = Queue()
        queue.addLast(100)
        queue.addLast(200)
        queue.addLast(300)
        assertEquals(300, queue.max())
    }

}
