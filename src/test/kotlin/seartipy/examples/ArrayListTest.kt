package seartipy.examples

import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue
import org.junit.Test as test

class ArrayListTest {
    @test
    fun addTest() {
        val arr = ArrayList()
        arr.add(100)
        assertEquals(1, arr.size)
        assertEquals(100, arr.first)
        assertEquals(listOf(100), arr.toList())
        arr.add(200)
        assertEquals(2, arr.size)
        assertEquals(listOf(100, 200), arr.toList())
        assertEquals(200, arr.last)
        assertEquals(100, arr.first)
        arr.add(300)
        arr.add(400)
        assertEquals(listOf(100, 200, 300, 400), arr.toList())
        assertEquals(4, arr.size)
        arr.add(500)
        assertEquals(listOf(100, 200, 300, 400, 500), arr.toList())
        assertEquals(5, arr.size)
    }

    @test
    fun popTest() {
        val arr = ArrayList()
        assertNull(arr.pop())
        assertTrue { arr.isEmpty() }
        arr.add(100)
        arr.pop()
        assertNull(arr.pop())
        assertEquals(0, arr.size)
        arr.add(200)
        arr.add(300)
        arr.pop()
        assertEquals(listOf(200), arr.toList())
        assertEquals(200, arr.first)
        arr.add(400)
        assertEquals(listOf(200, 400), arr.toList())
        arr.pop()
        assertEquals(200, arr.last)
        assertEquals(1, arr.size)
        arr.pop()
        arr.pop()
        assertNull(arr.pop())
    }

    @test
    fun insertAtTest() {
        val arr = ArrayList()
        arr.add(1)
        assertEquals(1, arr.size)
        arr.insertAt(30,0)
        assertEquals(listOf(30, 1), arr.toList())
        arr.insertAt(40,1)
        assertEquals(3, arr.size)
        assertEquals(30, arr.first)
        assertEquals(1, arr.last)
        assertEquals(listOf(30, 40, 1), arr.toList())
        arr.insertAt(60, 1)
        assertEquals(60, arr.get(1))
        assertEquals(listOf(30, 60, 40, 1), arr.toList())
        assertEquals(4, arr.size)
        arr.insertAt(50,2)
        assertEquals(5, arr.size)
        assertEquals(30, arr.first)
        assertEquals(1, arr.last)
    }

    @test
    fun removeAtTest() {
        val arr = ArrayList()
        arr.add(10)
        assertEquals(10,arr[0])
        assertEquals(1, arr.size)
        assertEquals(10,arr.removeAt(0))
        assertNull(arr.pop())
        arr.add(100)
        assertEquals(100, arr.get(0))
        arr.add(300)
        assertEquals(100, arr.first)
        arr.add(400)
        arr.add(600)
        assertEquals(600, arr.last)
        assertEquals(4, arr.size)
        assertEquals(400,arr.removeAt(2))
        assertEquals(3, arr.size)
        assertEquals(listOf(100,300,600),arr.toList())
        assertEquals(300, arr.removeAt(1))
        assertEquals(listOf(100,600),arr.toList())
    }

    @test
    fun getTest() {
        val arr = ArrayList()
        arr.add(100)
        assertEquals(1, arr.size)
        assertEquals(100,arr.get(0))
        arr.add(200)
        arr.add(300)
        assertEquals(100, arr.first)
        assertEquals(300,arr.get(2))
        arr.pop()
        assertEquals(200, arr.last)
        assertEquals(listOf(100, 200), arr.toList())
        assertEquals(200, arr.get(1))
        assertEquals(2, arr.size)
        arr.add(500)
        assertEquals(3, arr.size)
        assertEquals(500, arr.get(2))
        assertEquals(listOf(100, 200, 500), arr.toList())
        assertEquals(500, arr.last)
    }

    @test
    fun setTest() {
        val arr = ArrayList()
        arr.add(100)
        arr.add(200)
        assertEquals(2,arr.size)
        assertEquals(listOf(100, 200), arr.toList())
        arr.set(0,300)
        assertEquals(listOf(300, 200), arr.toList())
        assertEquals(2,arr.size)
        arr.add(400)
        assertEquals(3, arr.size)
        assertEquals(listOf(300, 200, 400), arr.toList())
        arr.set(2, 600)
        assertEquals(listOf(300, 200, 600), arr.toList())
        arr.pop()
        assertEquals(2, arr.size)
        arr.set(1, 700)
        assertEquals(listOf(300, 700), arr.toList())

    }

    @test
    fun toListTest() {
        val arr = ArrayList()
        assertTrue { arr.isEmpty() }
        arr.add(1)
        assertEquals(1, arr.size)
        assertEquals(listOf(1), arr.toList())
        arr.add(2)
        assertEquals(2, arr.size)
        assertEquals(listOf(1, 2), arr.toList())
        arr.insertAt(3, 1)
        assertEquals(listOf(1, 3, 2), arr.toList())
        arr.removeAt(1)
        assertEquals(2, arr.size)
        assertEquals(listOf(1, 2), arr.toList())
        arr.insertAt(4, 1)
        assertEquals(3, arr.size)
        assertEquals(listOf(1, 4, 2), arr.toList())
    }

}

