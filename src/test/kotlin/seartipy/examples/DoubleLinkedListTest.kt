package seartipy.examples

import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue
import org.junit.Test as test

class DoubleLinkedListTest {
    @test
    fun addFirstTest() {
        val list = DoubleLinkedList()
        assertTrue {list.isEmpty()}
        assertEquals(0, list.size())
        list.addFirst(100)
        list.addFirst(200)
        assertEquals(2, list.size())
        list.addFirst(300)
        assertEquals(3, list.size())
        assertEquals(listOf(300, 200, 100), list.toList())
    }

    @test
    fun addLast() {
        val list = DoubleLinkedList()
        assertTrue{list.isEmpty()}
        assertEquals(0, list.size())
        list.addLast(10)
        assertEquals(1, list.size())
        list.addLast(20)
        assertEquals(2, list.size())
        list.addLast(30)
        assertEquals(3, list.size())
        assertEquals(listOf(10, 20, 30), list.toList())

    }

    @test
    fun removeFirstTest() {
        val list = DoubleLinkedList()
        assertTrue { list.isEmpty() }
        list.addFirst(1)
        list.addFirst(2)
        assertEquals(2, list.size())
        assertEquals(2, list.removeFirst())
        assertEquals(1, list.removeFirst())
        assertEquals(0, list.size())
        assertNull(list.removeFirst())
                   }

    @test
    fun removeLastTest() {
        val list = DoubleLinkedList()
        assertTrue { list.isEmpty() }
        list.addFirst(10)
        list.addFirst(20)
        list.addLast(30)
        assertEquals(3, list.size())
        assertEquals(30, list.removeLast())
        assertEquals(10, list.removeLast())
        assertEquals(1, list.size())
        assertEquals(20, list.removeLast())
        assertEquals(0, list.size())
        assertNull(list.removeLast())
    }

    @test
    fun toList() {
        val list = DoubleLinkedList()
        assertTrue { list.isEmpty() }
        assertEquals(0, list.size())
        list.addFirst(100)
        assertEquals(1, list.size())
        list.addFirst(200)
        assertEquals(listOf(200, 100), list.toList())
        assertEquals(2, list.size())
        list.addLast(300)
        list.addLast(400)
        assertEquals(4, list.size())
        assertEquals(listOf(200, 100, 300, 400), list.toList())
        list.addFirst(500)
        assertEquals(listOf(500, 200, 100, 300, 400), list.toList())
        assertEquals(5, list.size())
        assertEquals(400, list.removeLast())
        assertEquals(500, list.removeFirst())
        assertEquals(3, list.size())
        assertEquals(listOf(200, 100, 300), list.toList())
    }

    @test
    fun nodeAtTest() {
        val list = DoubleLinkedList()
        assertTrue { list.isEmpty() }
        list.addLast(2)
        list.addFirst(1)
        assertEquals(2, list.size())
        assertEquals(1, list.nodeAt(0)?.data)
        assertEquals(2, list.nodeAt(1)?.data)
        assertNull(list.nodeAt(2))
            }
    @test
    fun findAtTest() {
        val list = DoubleLinkedList()
        assertNull(list.findAt(100))
        list.addFirst(0)
        list.addLast(1)
        assertEquals(2, list.size())
        assertNull(list.findAt(10))
        assertEquals(1, list.findAt(1)?.data)
        list.addLast(2)
        list.addLast(3)
        assertNull(list.findAt(4))
    }

    @test
    fun insertTest() {
        val list = DoubleLinkedList()
        list.addFirst(100)
        assertEquals(listOf(100), list.toList())
        list.insert(200, list.firstNode!!)
        assertEquals(listOf(200, 100),list.toList())
        list.insert(300, list.nodeAt(1)!!)
        assertEquals(listOf(200, 300, 100),list.toList())
        list.insert(500, list.nodeAt(2)!!)
        assertEquals(listOf(200, 300, 500, 100),list.toList())
        assertEquals(4,list.size())
    }

    @test
    fun removeTest() {
        val list = DoubleLinkedList()
        list.addFirst(4)
        list.addLast(5)
        assertEquals(2, list.size())
        assertEquals(listOf(4,5),list.toList())
        assertEquals(4,list.remove(list.nodeAt(0)!!))
        assertEquals(1, list.size())
        assertEquals(listOf(5),list.toList())
        assertEquals(1, list.size())
        assertEquals(5,list.remove(list.nodeAt(0)!!))
        assertEquals(0, list.size())
    }

    @test
    fun insertAfterTest() {
        val list = DoubleLinkedList()
        list.addFirst(100)
        assertEquals(1, list.size())
        assertEquals(listOf(100), list.toList())
        list.insertAfter(200, list.nodeAt(0)!!)
        assertEquals(2, list.size())
        assertEquals(listOf(100, 200), list.toList())
        list.insertAfter(300, list.nodeAt(0)!!)
        assertEquals(listOf(100, 300, 200), list.toList())
        list.insertAfter(400, list.nodeAt(1)!! )
        assertEquals(listOf(100, 300, 400, 200), list.toList())
    }
}